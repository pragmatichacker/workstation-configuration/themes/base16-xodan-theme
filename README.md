# Xodan Scheme for base16

Pleasing punchy colors for a dark color background.

Follows standards as laid out in [the upstream Base16 project](https://github.com/chriskempson/base16).
